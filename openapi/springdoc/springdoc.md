## Springdoc-openapi 模块
![springdoc-openapi_modules](springdoc-openapi_modules.png)

## Springdoc-openapi 属性

| 参数名称 | 默认值 | 描述 |
| --- | --- | --- |
|springdoc.api-docs.path |`/v3/api-docs`|`String`, 用于自定义 Json 格式的 OpenAPI 文档路径。 |
|springdoc.api-docs.enabled |`true`|`Boolean`. 禁用 springdoc-openapi 端点（默认为 /v3/api-docs）。 |
|springdoc.packages-to-scan|`*`|`List of Strings`.要扫描的包列表（逗号分隔）|
|springdoc.paths-to-match|`/*`|`List of Strings`.要匹配的路径列表（逗号分隔）|
| springdoc.produces-to-match|`/*`|`List of Strings`.要匹配的生产媒体类型列表（逗号分隔）|
|springdoc.headers-to-match|`/*`|`List of Strings`.要匹配的标题列表（逗号分隔）|
|springdoc.consumes-to-match|`/*`|`List of Strings`.要匹配的消耗媒体类型列表（逗号分隔）|
|springdoc.paths-to-exclude|  |`List of Strings`.要排除的路径列表（逗号分隔）|
|springdoc.packages-to-exclude|  |`List of Strings`.要排除的包列表（逗号分隔）|
|springdoc.default-consumes-media-type|`application/json`|`String`. 默认使用媒体类型。|
|springdoc.default-produces-media-type|`**/**`|`String`.默认产生媒体类型。|
|springdoc.cache.disabled|`false`|`Boolean`. 禁用计算出来的 OpenAPI 的 springdoc-openapi 缓存。|
|springdoc.show-actuator|`false`|`Boolean`. 显示执行器端点。|
|springdoc.auto-tag-classes|`true`|`Boolean`. 禁用 springdoc-openapi 自动标签。|
|springdoc.model-and-view-allowed|`false`|`Boolean`. 允许带有 ModelAndView 的 RestControllers 返回出现在 OpenAPI 描述中。|
|springdoc.override-with-generic-response|`true`|`Boolean`. 当为 true 时，自动将 @ControllerAdvice 响应添加到所有生成的响应中。|
|springdoc.api-docs.groups.enabled|`true`|`Boolean`. 禁用 springdoc-openapi 组。|
|springdoc.group-configs\[0\].group|  |`String`.组名|
|springdoc.group-configs\[0\].packages-to-scan|`*`|`List of Strings`.要扫描的包列表（逗号分隔）|
|springdoc.group-configs\[0\].paths-to-match|`/*`|`List of Strings`. 匹配组的路径列表（逗号分隔）|
|springdoc.group-configs\[0\].paths-to-exclude|\`\`|`List of Strings`.要排除的路径列表（逗号分隔）|
|springdoc.group-configs\[0\].packages-to-exclude|  |`List of Strings`.要排除的包列表（逗号分隔）|
|springdoc.group-configs\[0\].produces-to-match|`/*`|`List of Strings`.要匹配的生产媒体类型列表（逗号分隔）|
|springdoc.group-configs\[0\].consumes-to-match|`/*`|`List of Strings`.要匹配的消耗媒体类型列表（逗号分隔）|
|springdoc.group-configs\[0\].headers-to-match|`/*`|`List of Strings`.要匹配的标题列表（逗号分隔）|
|springdoc.webjars.prefix|`/webjars`|`String`, 要更改可见的 webjars 前缀，请更改 spring-webflux 的 swagger-ui 的 URL。|
|springdoc.api-docs.resolve-schema-properties|`false`|`Boolean`. 在@Schema 上启用属性解析器（名称、标题和描述）。|
|springdoc.remove-broken-reference-definitions|`true`|`Boolean`. 禁止删除损坏的参考定义。|
|springdoc.writer-with-default-pretty-printer|`false`|`Boolean`. 启用 OpenApi 规范的漂亮打印。|
|springdoc.model-converters。弃用-converter.enabled|`true`|`Boolean`. 禁用弃用模型转换器。|
|springdoc.use-fqn|`false`|`Boolean`. 启用完全限定名称。|
|springdoc.show-login-endpoint|`false`|`Boolean`. 使 spring 安全登录端点可见。|
|springdoc.pre-loading-enabled|`false`|`Boolean`. 在应用程序启动时加载 OpenAPI 的预加载设置。|
|springdoc.writer-with-order-by-keys|`false`|`Boolean`. 启用确定性/字母顺序。|
|springdoc.use-management-port|`false`|`Boolean`. 在执行器管理端口上公开 swagger-ui。|
|springdoc.enable-native-image-support|`false`|`Boolean`. 强制启用本机图像支持。|

## 参考文档
- 官网：https://springdoc.org/