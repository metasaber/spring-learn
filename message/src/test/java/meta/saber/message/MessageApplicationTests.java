package meta.saber.message;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;

@SpringBootTest
class MessageApplicationTests {
    @Test
    void i18n() {
        String message1 = messageSource.getMessage("i18n.key1", null, null);
        System.out.println(message1);

        String message2 = messageSource1.getMessage("i18n.key1", null, null);
        System.out.println(message2);
    }

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private MessageSource messageSource1;
}
