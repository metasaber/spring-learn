package meta.saber.message.i18n;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class I18nRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        String message1 = messageSource.getMessage("i18n.key1", null, null);
        System.out.println(message1);
    }

    private MessageSource messageSource;
    @Autowired
    public void set(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
}
