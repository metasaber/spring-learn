package meta.saber.message.domain1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class Domain1Runner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        String message1 = messageSource.getMessage("domain1.key1", null, null);
        System.out.println(message1);
    }

    private MessageSource messageSource;
    @Autowired
    public void set(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
}
