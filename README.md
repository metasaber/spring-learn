## 打包运行命令
- Maven：
```
./mvnw clean package spring-boot:run -Dmaven.test.skip=true

./mvnw clean package -Dmaven.test.skip=true
```
- Gradle：
```
./gradlew clean bootRun -x test

./gradlew clean build -x test
```