package meta.saber.panorama.photo_sphere_viewer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("psv/example1")
public class Example1Controller {
    @RequestMapping
    public String example1() {
        return "photo-sphere-viewer/example1.html";
    }
}
