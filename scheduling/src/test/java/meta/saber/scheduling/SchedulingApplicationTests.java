package meta.saber.scheduling;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Async;

@SpringBootTest
@Slf4j
class SchedulingApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    @Async("taskExecutor")
    void async() {
        log.debug("async线程：" + Thread.currentThread().getName());
    }

}
