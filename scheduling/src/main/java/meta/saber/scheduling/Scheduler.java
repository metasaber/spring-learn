package meta.saber.scheduling;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author SwordZhou
 * @since 2020-12-18
 */
@EnableScheduling
@Component
public class Scheduler {
    //@Scheduled(fixedDelay = 10000L)
    public void hello() {
        System.out.println("hello");
    }
}
