package meta.saber.scheduling.async;

import meta.saber.scheduling.async.service.B;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;

/**
 * @author SwordZhou
 * @since 2020-12-4
 */
@Component
public class AsyncRunner implements CommandLineRunner {
    private B b;

    @Autowired
    public void set(B b) {
        this.b = b;
    }

    //@Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.initialize();
        return executor;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println(Thread.currentThread().getName() + " : AsyncRunner->run()");
        b.b();
    }
}
