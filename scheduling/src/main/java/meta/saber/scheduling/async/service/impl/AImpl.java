package meta.saber.scheduling.async.service.impl;

import meta.saber.scheduling.async.service.A;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author SwordZhou
 * @since 2020-12-4
 */
@Service
public class AImpl implements A {
    @Async
    @Override
    public void a() {
        System.out.println(Thread.currentThread().getName() + " : A->a()");
    }
}
