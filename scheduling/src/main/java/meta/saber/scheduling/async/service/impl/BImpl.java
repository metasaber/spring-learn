package meta.saber.scheduling.async.service.impl;

import meta.saber.scheduling.async.service.A;
import meta.saber.scheduling.async.service.B;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author SwordZhou
 * @since 2020-12-4
 */
@Service
public class BImpl implements B {
    @Async
    @Override
    public void b() {
        System.out.println(Thread.currentThread().getName() + " : B->b()");
        a.a();
    }

    private A a;
    @Autowired
    public void set(A a) {
        this.a = a;
    }
}
